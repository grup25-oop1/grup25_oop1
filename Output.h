#pragma once
#include<string>
#include <fstream>
#include <iostream>
using namespace std;
class Output
{
private:
	static fstream file;
public:
	/**
	*@brief:This function opens file.
	*/
	void openFileFirst(string);
	/**
	*@brief:This function writes the given string to the file.
	*@param:string str
	*/
	static void writeLine(string str)
	{
		file << str;
	};
};