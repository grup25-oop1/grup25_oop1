#include "Engine.h"
#include <iomanip>
Engine::Engine(bool status) :fuel_per_second(5.5),consumedFuel(0.0), randomTank(0), connectedTankCount(0),valveStatus(0),howManyRandom(0)
{
   this->status=status;
   internal.set_fuel_quantity(55.0);
}

Engine::Engine():fuel_per_second(5.5), status(0),consumedFuel(0.0), randomTank(0), connectedTankCount(0), valveStatus(0), howManyRandom(0)
{
}

Engine::~Engine()
{
}

void Engine::start_engine()
{
    if (connectedTankCount > 0)
    {
        status = 1;
        internal.set_fuel_quantity(55.0);
        Output::writeLine("\nEngine is starting....");
    }
    else
    {
        Output::writeLine("\nYou need connect tank to start engine.");
    }
}

void Engine::stop_engine()
{
    fuelBeforeEngineStop();
    status = 0;
    Output::writeLine("\nEngine is stopped.");
}

void Engine::set_status(bool& status)
{
    this->status = status;
}

bool Engine::get_status() const
{
    return status;
}

int Engine::findIndexOfTank(int tankId)
{
   int index;
   for(int i=0;i<tanks.size();i++)
   {   
      if(tanks.at(i)->get_id()==tankId)
      {
         index=i;
         return index;
      }
   }
   return -1;
} 

bool Engine::isThereTank(int tankId)
{
    if (findIndexOfTank(tankId) == -1)
    {
        return false;
    }
    else
        return true;
}

void Engine::connect_fuel_tank_to_engine(int tankId)
{
    int index = 0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        if (tanks.at(index)->getTankStatus()==1)
        {
            Output::writeLine("\nTank[");Output::writeLine(to_string(tankId));
            Output::writeLine("] has already connected.");
        }
        else
        {
            Output::writeLine("\nTank[");Output::writeLine(to_string(tankId));
            Output::writeLine("] connected to engine.");
            tanks.at(index)->setTankStatus(1);
            connectedTankCount++;
        }
    }
    else
    {
        Output::writeLine("\nThere is no such Tank[");Output::writeLine(to_string(tankId));
        Output::writeLine("].");
    }
}

bool Engine::is_connected(int tankId) 
{
    int index = 0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        if (tanks.at(index)->getTankStatus()==1)
        {
            return true;
        }
        return false;
    }
    else
    {
        Output::writeLine("\nThere is no such Tank[");Output::writeLine(to_string(tankId));
        Output::writeLine("].");
        return false;
    }
}

void Engine::disconnect_fuel_tank_to_engine(int tankId)
{
    int index = 0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        if (tanks.at(index)->getTankStatus() == 0)
        {
            Output::writeLine("\nTank["); Output::writeLine(to_string(tankId));
            Output::writeLine("] has already disconnected.\n");
        }
        else
        {
            Output::writeLine("\nFuel Tank["); Output::writeLine(to_string(tankId));
            Output::writeLine("] disconnected to engine.\n");
            tanks.at(index)->setTankStatus(0);
            connectedTankCount--;
        }
    }
    else
    {
        Output::writeLine("\nThere is no such Tank["); Output::writeLine(to_string(tankId));
        Output::writeLine("]. You can't disconnect.");
    }
}

void Engine::list_connected_tanks()
{
    Output::writeLine("\nConnected tank count:"); Output::writeLine(to_string(connectedTankCount));
    Output::writeLine("\n|----------Connected Tanks----------|\n");
    for (int i = 0; i < tanks.size(); i++)
    {
        if (is_connected(tanks.at(i)->get_id()))
        {
            Output::writeLine("\tTank["); Output::writeLine(to_string(tanks.at(i)->get_id()));
            Output::writeLine("]\n");
        }
    }
    Output::writeLine("\n|-----------------------------------|\n");
}

void Engine::print_total_fuel_quantity()
{
    double sum = 0.0;
    Output::writeLine("\n----------Total Fuel Quantity----------\n");
    cout << "----------Total Fuel Quantity----------" << endl;
    for (int i = 0; i < tanks.size(); i++)
    {
        Output::writeLine("Tank["); Output::writeLine(to_string(tanks.at(i)->get_id()));
        Output::writeLine("]"); Output::writeLine(to_string(tanks.at(i)->get_fuel_quantity()));
        Output::writeLine("\n");
        sum += tanks.at(i)->get_fuel_quantity();
    }
    Output::writeLine("Total fuel-->"); Output::writeLine(to_string(sum));
}

void Engine::print_total_consumed_fuel_quantity()
{
    Output::writeLine("\nConsumed fuel quantity:"); Output::writeLine(to_string(consumedFuel));
}

void Engine::print_fuel_tank_count()
{
    Output::writeLine("\nFuel tank count: "); Output::writeLine(to_string(tanks.size()));
}

void Engine::add_fuel_tank(double capacity)
{
    Tank* tank = new Tank(capacity);
    Valve* valve = new Valve();
    this->addObserver(tank);
    this->tanks.push_back(tank);
    this->observs2.push_back(valve);
    Output::writeLine("\nTank["); Output::writeLine(to_string(tank->get_id()));
    Output::writeLine("] added and capacity is "); Output::writeLine(to_string(capacity));
}

void Engine::list_fuel_tanks() const
{
    Output::writeLine("\n|----------Fuel Tanks----------|\n");
    for (int i = 0; i < tanks.size(); i++)
    {
        Output::writeLine("\tTank["); Output::writeLine(to_string(tanks.at(i)->get_id()));
        Output::writeLine("]\n");
    }
    Output::writeLine("|------------------------------|\n");
}

void Engine::remove_fuel_tank(int tankId)
{
    int index=0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        Output::writeLine("\nTank["); Output::writeLine(to_string(tankId));
        Output::writeLine("] removed.");
        if (tanks.at(index)->getTankStatus()==1)
        {
            connectedTankCount--;
        }
        tanks.erase(tanks.begin() + index);
        this->removeObserve(tankId);
    }
    else
    {
        Output::writeLine("\nThere is no such Tank["); Output::writeLine(to_string(tankId));
        Output::writeLine("]. You can't remove.");
    }
}

void Engine::print_tank_info(int tankId)
{
    int index = 0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        Output::writeLine("\n|----------Tank["); Output::writeLine(to_string(tankId));
        Output::writeLine("] Info----------|\n");
        Output::writeLine("Capacity: "); Output::writeLine(to_string(tanks.at(index)->get_capacity()));
        Output::writeLine("\nFuel quantity: "); Output::writeLine(to_string(tanks.at(index)->get_fuel_quantity()));
        Output::writeLine("\nIs tank broken? "); Output::writeLine(to_string(tanks.at(index)->get_broken()));
        Output::writeLine("\nIs tank's valve is open? "); Output::writeLine(to_string(tanks.at(index)->getValveStatus()));
        Output::writeLine("\nIs tank connected? "); Output::writeLine(to_string(tanks.at(index)->getTankStatus()));
        Output::writeLine("\n|--------------------------------|");
    }
    else
    {
        Output::writeLine("\nThere is no such Tank["); Output::writeLine(to_string(tankId));
        Output::writeLine("].");
    }
}

void Engine::break_fuel_tank(int tankId)
{
    int index = 0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        if (tanks.at(index)->get_broken()==1)
        {
            Output::writeLine("\nTank["); Output::writeLine(to_string(tankId));
            Output::writeLine("] has already broken.");
        }
        else
        {
            Output::writeLine("\nTank["); Output::writeLine(to_string(tankId));
            Output::writeLine("] is broked.");
            tanks.at(index)->set_broken(1);
        }
    }
    else
    {
        Output::writeLine("\nThere is no such Tank["); Output::writeLine(to_string(tankId));
        Output::writeLine("]. You can't break.");
    }
}

void Engine::repair_fuel_tank(int tankId)
{
    int index = 0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        if (tanks.at(index)->get_broken()==0)
        {
            Output::writeLine("\nTank["); Output::writeLine(to_string(tankId));
            Output::writeLine("] is not broken.");
        }
        else
        {
            tanks.at(index)->set_broken(0);
            Output::writeLine("\nTank["); Output::writeLine(to_string(tankId));
            Output::writeLine("] is repaired.");
        }
    }
    else
    {
        Output::writeLine("\nThere is no such Tank["); Output::writeLine(to_string(tankId));
        Output::writeLine("]. You can't repair.");
    }
}

void Engine::openValve(int tankId)
{
    int index = 0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        Output::writeLine("\nTank["); Output::writeLine(to_string(tankId));
        Output::writeLine("] valve is opening...");
        tanks.at(index)->setValveStatus(1);
        countOpenValve++;
    }
    else
    {
        Output::writeLine("\nThere is no such Tank["); Output::writeLine(to_string(tankId));
        Output::writeLine("]");
    }
}

void Engine::closeValve(int tankId)
{
    int index = 0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        Output::writeLine("\nTank["); Output::writeLine(to_string(tankId));
        Output::writeLine("] valve is closing...");
        tanks.at(index)->setValveStatus(0);
        countOpenValve--;
    }
    else
    {
        Output::writeLine("\nThere is no such Tank["); Output::writeLine(to_string(tankId));
        Output::writeLine("].");
    }
}

void Engine::fillTank(int tankId, double fuel_quantity)
{
    int index = 0;
    if (isThereTank(tankId))
    {
        index = findIndexOfTank(tankId);
        if (tanks.at(index)->get_fuel_quantity() + fuel_quantity > tanks.at(index)->get_capacity())
        {
            Output::writeLine("\nYou exceeded the capacity of Tank["); Output::writeLine(to_string(tankId));
            Output::writeLine("] you can not fill.");
        }
        else
        {
            Output::writeLine("\nFill Tank["); Output::writeLine(to_string(tankId));
            Output::writeLine("] --- > fuel quantity:"); Output::writeLine(to_string(fuel_quantity));
            tanks.at(index)->set_fuel_quantity(fuel_quantity);
        }
    }
    else
    {
        Output::writeLine("\nThere is no such Tank["); Output::writeLine(to_string(tankId));
        Output::writeLine("]. You can't fill.");
    }
}

void Engine::wait(double second)
{
    for (int i = 0; i < second; i++)
    {
        absorbFuel();
    }
}

void Engine::absorbFuel()
{
    int index=0;
    if (status == 1)
    {
        if (internal.get_fuel_quantity() > 20)
        {
            Output::writeLine("\nAbsorbing fuel in internal tank.");
            internal.set_fuel_quantity(internal.get_fuel_quantity() - 5.5);
            consumedFuel += fuel_per_second;
        }
        else// if internal tank capacity goes below 20
        {
            if (connectedTankCount > 0 && countOpenValve > 0)
            {
                if (howManyRandom == 0)
                {
                    index = randomSelectTank();
                    if (index == -1)
                    {
                        Output::writeLine("\nThere is no fuel.");
                    }
                    else
                    {
                        Output::writeLine("\nInternal Tank fuel quantity is "); Output::writeLine(to_string(internal.get_fuel_quantity()));
                        Output::writeLine(" so another tank is selecting.");
                        Output::writeLine("\nTank["); Output::writeLine(to_string(randomTank));
                        Output::writeLine("] selected randomly.");
                        (tanks.at(index))->set_fuel_quantity((tanks.at(index))->get_fuel_quantity() - 5.5);
                        consumedFuel += fuel_per_second;
                    }
                }
                else
                {
                    index = findIndexOfTank(randomTank);
                    if (tanks.at(index)->get_fuel_quantity() >= 5.5)
                    {
                        Output::writeLine("\nContinues to absorbation from Tank["); Output::writeLine(to_string(randomTank));
                        Output::writeLine("].");
                        (tanks.at(index))->set_fuel_quantity((tanks.at(index))->get_fuel_quantity() - 5.5);
                        consumedFuel += fuel_per_second;
                    }
                    else
                    {
                        index = randomSelectTank();
                        if (index == -1)
                        {
                            Output::writeLine("\nThere is no fuel.");
                        }
                        else
                        {
                            (tanks.at(index))->set_fuel_quantity((tanks.at(index))->get_fuel_quantity() - 5.5);
                            consumedFuel += fuel_per_second;
                        }
                    }
                }
            }
            else
            {
                Output::writeLine("\nThere is no connected tank or tank's valve not open in this engine for absorbation.");
            }
        }
    }
}

int Engine::randomSelectTank()
{
    int index = 0;
    int count = 0;
    srand(time(NULL));
    for (int i = 0; i < tanks.size(); i++)
    {
        if (tanks.at(i)->get_fuel_quantity() >= 5.5 && tanks.at(i)->getTankStatus()==1
            && tanks.at(i)->getValveStatus()==1 && tanks.at(i)->get_broken()==0)
        {
            count++;
        }
    }
    if (count == 0)
    {
        return -1;
    }
    while (1)
    {
        randomTank = tanks.at((rand() % tanks.size()))->get_id();
        index = findIndexOfTank(randomTank);
        double fuel_quantity_of_selected_tank = tanks.at(index)->get_fuel_quantity();
        if (tanks.at(index)->getTankStatus() == 1 && tanks.at(index)->getValveStatus() == 1
            && tanks.at(index)->get_broken() == 0 && fuel_quantity_of_selected_tank >= 5.5)
        {
            howManyRandom++;
            return index;
            break;
        }
    }
}

void Engine::give_back_fuel(double quantity)
{
    internal.set_fuel_quantity(internal.get_fuel_quantity()-quantity);
    int index=randomSelectTank();
    int id = tanks.at(index)->get_id();
    Output::writeLine("\nTank["); Output::writeLine(to_string(id));
    Output::writeLine("] selected randomly for giving back fuel.");
    tanks.at(index)->set_fuel_quantity(quantity);
}

void Engine::fuelBeforeEngineStop()
{
    double min = tanks.at(0)->get_fuel_quantity();
    double internalFuel;
    int minIndex = 0;
    if (connectedTankCount > 0)
    {
        for (int i = 1; i < tanks.size(); i++)
        {
            if (is_connected(tanks.at(i)->get_id()))
            {
                if (tanks.at(i)->get_fuel_quantity() < min)
                {
                    min = tanks.at(i)->get_fuel_quantity();
                    minIndex = i;
                }
            }
        }
        internalFuel = internal.get_fuel_quantity();
        Output::writeLine("\nTank["); Output::writeLine(to_string(tanks.at(minIndex)->get_id()));
        Output::writeLine("] fuel is"); Output::writeLine(to_string(tanks.at(minIndex)->get_fuel_quantity()));
        Output::writeLine(" and this minumum fuel.\n"); Output::writeLine(to_string(internalFuel));
        Output::writeLine(" return fuel in its internal tank to the Tank["); Output::writeLine(to_string(tanks.at(minIndex)->get_id()));
        Output::writeLine("] connected tank.\n");
        tanks.at(minIndex)->set_fuel_quantity(min + internalFuel);
        internal.set_fuel_quantity(0);
    }
    else
    {
        Output::writeLine("\nThere is no connected tank here to return fuel");
        Output::writeLine("in its internal tank to the connected tanks before it is stopped.");
    }
}

void Engine::addObserver(Observer* obs)
{
    this->observs.push_back(obs);
}

void Engine::removeObserve(int index)
{
    this->observs[index-1] = NULL;
    this->observs2[index-1] = NULL;
}

void Engine::simulationStop()
{
    this->notify();
}

void Engine::update()
{
    Output::writeLine("\nSimulation stop.");
}

void Engine::notify()
{
    update();
    for (int i = 0; i < observs.size(); i++)
    {
        if (observs[i] != NULL)
        {
            observs[i]->update(i+1);
            observs2[i]->update(i+1);
        }
    }
}
