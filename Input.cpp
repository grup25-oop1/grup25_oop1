#include<iostream>
#include<fstream>
#include<string>
#include "Input.h"
#include "Engine.h"
#include "Tank.h"
#include "Valve.h"

using namespace std;

void Input::is_command(fstream& commands)
{
	string str;
	int temp;
	Engine engine;
	Tank tank;
	if (!commands.is_open())
	{
		cout << "File does not exist!" << endl;
		system("pause");
		exit(0);
	}
	if (commands.peek() == ifstream::traits_type::eof())
	{
		cout << "File is empty!" << endl;
		system("pause");
		exit(0);
	}

	while (commands >> str)
	{
		if (str == "start_engine;")
		{
			if (engine.get_status() == 1)
			{
				Output::writeLine("\nEngine has already started.");
			}
			else
			{
				engine.start_engine();
			}
		}
		else if (str == "stop_engine;")
		{
		  engine.stop_engine();
		}
		else if (str == "give_back_fuel")
		{
		  commands>>str;
		  temp=stoi(str);
		  engine.give_back_fuel(temp);
		}
		else if (str == "add_fuel_tank")
		{
		  commands>>str;
		  temp=stoi(str);
		  engine.add_fuel_tank(temp);
		}
		else if (str == "list_fuel_tanks;") 
		{
			engine.list_fuel_tanks();
		}
		else if (str == "remove_fuel_tank")
		{
				commands >> str;
				temp = stoi(str);
				engine.remove_fuel_tank(temp);
		}
		else if (str == "connect_fuel_tank_to_engine")
		{
			commands >> str;
			temp = stoi(str);
			engine.connect_fuel_tank_to_engine(temp);
		}
		else if (str == "disconnect_fuel_tank_from_engine")
		{
			commands >> str;
			temp = stoi(str);
			engine.disconnect_fuel_tank_to_engine(temp);
		}
		else if (str == "open_valve")
		{
			commands >> str;
			temp = stoi(str);
			engine.openValve(temp);
		}
		else if (str == "close_valve")
		{
			commands >> str;
			temp = stoi(str);
			engine.closeValve(temp);
		}
		else if (str == "break_fuel_tank")
		{
		  commands>>str;
		  temp=stoi(str);
		  engine.break_fuel_tank(temp);
		}
		else if (str == "repair_fuel_tank")
		{
		  commands>>str;
		  temp=stoi(str);
		  engine.repair_fuel_tank(temp);
		}
		else if (str == "fill_tank")
		{
		  commands>>str;
		  temp=stoi(str);
		  commands>>str;
		  int temp2=stoi(str);
		  engine.fillTank(temp,temp2);
		}
		else if (str == "wait")
		{
		  commands>>str;
		  temp=stoi(str);
		  engine.wait(temp);
		}
		else if(str=="print_fuel_tank_count;")
		{
			engine.print_fuel_tank_count();
		}
		else if(str=="list_connected_tanks;")
		{
			engine.list_connected_tanks(); 
		}
		else if(str=="print_total_fuel_quantity;")
		{
			engine.print_total_fuel_quantity(); 
		}
		else if(str=="print_total_consumed_fuel_quantity;")
		{
			engine.print_total_consumed_fuel_quantity();
		}
		else if (str == "print_tank_info")
		{
			commands >> str;
			temp = stoi(str);
			engine.print_tank_info(temp);
		}
		else if (str == "stop_simulation;")
		{
			engine.simulationStop();
			exit(0);
		}
		else
		{
			cout << "This command does not exist!" << endl;
		}
	}
	commands.close();
}


