#pragma once
class Observer
{
public:
	virtual void update(int) = 0;
};

