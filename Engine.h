#pragma once
#include <iostream> 
#include <vector>
#include <time.h>
#include "Tank.h"
#include "Subject.h"
#include "Observer.h"
#include"Input.h"
#include"Output.h"
#include<string>
using namespace std;
class Engine:public Subject
{
private:
    const double fuel_per_second;
    bool status;
    std::vector<Tank*>tanks;
    vector <Observer*>observs;
    vector <Observer*>observs2;
    Tank internal;
    int randomTank;
    int howManyRandom;
    double consumedFuel;
    int connectedTankCount;
    int countOpenValve=0;
    bool valveStatus;

public:
    Engine(bool);
    Engine();
    ~Engine();
    void start_engine();
    void stop_engine();
    void set_status(bool&);
    bool get_status() const;
    int findIndexOfTank(int);
    bool isThereTank(int tankId);
    void connect_fuel_tank_to_engine(int);
    void disconnect_fuel_tank_to_engine(int);
    void list_connected_tanks();
    void print_total_fuel_quantity();
    void print_total_consumed_fuel_quantity();
    bool is_connected(int);
    void print_fuel_tank_count();
    void add_fuel_tank(double);
    void list_fuel_tanks()const;
    void remove_fuel_tank(int);
    void break_fuel_tank(int);
    void repair_fuel_tank(int);
    void print_tank_info(int _id);
    void openValve(int tankId); 
    void closeValve(int tankId); 
    void fillTank(int, double);
    void wait(double);
    void absorbFuel();
    int randomSelectTank();
    void give_back_fuel(double);
    void fuelBeforeEngineStop();
    void addObserver(Observer* obs);
    void removeObserve(int);
    void simulationStop();
    void update();
    void notify();
};