#include "Valve.h"
#include "Engine.h"
#include <iostream>
using namespace std;
Valve::Valve()
{}
/**
*@brief:Constructor-2 for Valve
*@param:int tankId
*/
Valve::Valve(int tankId)
{
	this->tankId = tankId;
	this->valveStatus = 0;
}
/**
*@brief:Destructor for Valve
*/
Valve::~Valve()
{
}
/**
*@brief:This function returns valve status.
*@return: bool valveStatus
*/
bool Valve::getValveStatus()const
{
	return valveStatus;
}

void Valve::setValveStatus(bool valveStatus)
{
	this->valveStatus = valveStatus;
}

void Valve::update(int id)
{
	Output::writeLine("\nValve ["); Output::writeLine(to_string(id));
	Output::writeLine("]: Stop simulation.");
}

