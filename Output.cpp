 #include <iostream>
#include "Output.h"
fstream Output::file;

void Output::openFileFirst(string outputFileName)
{
	file.open(outputFileName, ios::out);
	if (!file.is_open())
	{
		cout << "This file does not exist." << endl;
		exit(0);
	}
}