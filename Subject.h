#pragma once
#include "Observer.h"
class Subject
{
	virtual void addObserver(Observer*) = 0;
	virtual void removeObserve(int) = 0;
	virtual void notify() = 0;
};

