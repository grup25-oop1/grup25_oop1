#include "Tank.h"
#include "Engine.h"
#include <iostream>
#include <string>
using namespace std;
int Tank::tankCounter = 0;
Tank::Tank()
{
    this->id = 0;
    this->capacity = 0.0;
    this->fuel_quantity = 0.0;
    this->broken=0;
    this->valveStatus = 0;
    this->tank_status = 0; 
}
/**
*@brief: Constructor2 for Tank.
 @param: double capacity
*/
Tank::Tank(double capacity):id(++tankCounter)
{
    this->capacity=capacity;
    this->fuel_quantity = 0.0;
    this->broken = 0;
    this->valveStatus = 0;
    this->tank_status = 0;
}
/**
*@brief:Destructor for Tank.
*/
Tank::~Tank()
{
}
/**
*@brief:This function sets tank id.
*@param: int id
*/
void Tank::set_id(int id)
{
    this->id = id;
}
/**
*@brief:This function gets tank id.
*@return:int id
*/
int Tank::get_id() const
{
    return id;
}
/**
*@brief:This function sets tank fuel capacity.
*@param: int capacity
*/
void Tank::set_capacity(int capacity)
{
    this->capacity = capacity;
}
/**
*@brief:This function gets tank fuel capacity.
*@return:int capacity
*/
int Tank::get_capacity() const
{
    return capacity;
}
/**
*@brief:This function sets tank fuel quantity.
*@param: int quantity
*/
void Tank::set_fuel_quantity(double quantity)
{
    fuel_quantity = quantity;
}
/**
*@brief:This function return fuel quantity.
*@return: double quantity
*/
double Tank::get_fuel_quantity() const
{
    return fuel_quantity;
}
/**
*@brief:This function sets broken.
*@param: bool broken
*/
void Tank::set_broken(bool broken)
{
    this->broken = broken;
}
/**
*@brief:This function returns broken.
*@return: bool broken
*/
bool Tank::get_broken() const
{
    return broken;
}
/**
*@brief: This function sets tank status.
*@param: int situation
*/
void Tank::setTankStatus(int situation)
{
    tank_status = situation;
}
/**
*@brief: This function gets tank status.
*@return: bool tank_status
*/
bool Tank::getTankStatus()const
{
    return tank_status;
}
void Tank::setValveStatus(bool valveStatus)
{
    this->valveStatus = valveStatus;
}
int Tank::getValveStatus()
{
    return valveStatus;
}

void Tank::update(int id)
{
    Output::writeLine("\nTank["); Output::writeLine(to_string(id));
    Output::writeLine("]: Stop simulation.");
}



