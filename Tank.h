#pragma once
#include <iostream>
#include "Valve.h"
#include "Observer.h"
#include"Output.h"
class Tank:public Observer
{
private:
    int id;
    double capacity;
    double fuel_quantity;
    bool broken;
    bool valveStatus;
    static int tankCounter;
    bool tank_status;
public:
    Tank();
    Tank(double);
    ~Tank();
    void set_id(int id);
    int get_id() const;
    void set_capacity(int capacity);
    int get_capacity() const;
    void set_fuel_quantity(double quantity);
    double get_fuel_quantity() const;
    void set_broken(bool);
    bool get_broken() const;
    void setTankStatus(int);
    bool getTankStatus() const;
    int getValveStatus();
    void update(int);
    void setValveStatus(bool valveStatus); 
    static int getCounter() {
        return tankCounter;
    };
};