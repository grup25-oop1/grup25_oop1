#pragma once
#include <iostream>
#include"Observer.h"
class Valve:public Observer
{
private:
	int tankId=0;
	bool valveStatus=0;
public:
	Valve();
	Valve(int);
	~Valve();
	bool getValveStatus()const;
	void setValveStatus(bool valveStatus);
	void update(int);
};
